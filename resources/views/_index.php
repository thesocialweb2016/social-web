<!DOCTYPE html>
<html lang="en">
<head>
	<title>Social Web 2016</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script type="text/javascript" src="<?= url(); ?>/js/timeline.js"></script>
	<script type="text/javascript" src="<?= url(); ?>/js/Chart.js"></script>
	<script type="text/javascript">
		var base_url = '<?= url(); ?>';
		$(document).ready(function () {
			setBaseUrl(base_url);
			getTimeline();
			getTimelineGraph();
			getTimelineGraphUserMention();
			getGraphDailyNumber();
			getGraphHashtags();
			// add a handler to the search button.
			$('#actionSearch').on('click', search);
		});
	</script>
</head>
<body>
<div class="container">
	<h2>Filter Tweets</h2>
	<form id="searchForm" class="form-horizontal" role="form">
		<div class="row">
			<div class="col-sm-4">
				<div class="form-group">
					<label class="control-label col-sm-4" for="keywords">Keywords:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="keywords" placeholder="Keywords (ex. 'strange')">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="owner">Owner:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="owner" placeholder="Owner (ex. 'togum')">
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="form-group">
					<label class="control-label col-sm-4" for="year">Year:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="year" placeholder="Year (ex. 2016)">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-4" for="location">Location:</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="year" placeholder="Location (ex. Amsterdam)">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group">
				<div class="col-sm-offset-4 col-sm-4">
					<button id="actionSearch" class="btn btn-primary btn-block"><b>Filter Tweets</b></button>
				</div>
			</div>
		</div>
	</form>

	<h2>Tweets</h2>
	<div id="tweets">
		<table class="table table-condensed">
			<thead>
			<tr>
				<th>Picture</th>
				<th>Tweet</th>
				<th>Date</th>
			</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<h2>Graph</h2>
	<div id="tweetsGraph" >
		<div class="row">
			<div class="col-sm-4">
				<p above="myChart">Number of Tweets</p>
				<canvas id="myChart" height="450" width="600"></canvas>
			</div>
			<div class="col-sm-10">
				<div id="container" style="width:600px; height:600px;"></div>
				<div id="line_chart"style="width:600px; height:600px;"></div>
				<div id="hashtags_chart" style="width:600px; height:600px;"></div>
			</div>
	
			
		</div>
	</div>
</div>
</body>
</html>