<!DOCTYPE html>
<html class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Network Institute Portal</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

	<!-- Fonts -->


	<!-- CSS -->

	<link rel="stylesheet" href="<?= url(); ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= url(); ?>/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= url(); ?>/css/main.css">


	<!-- Js -->
	<script src="js/vendor/modernizr-2.6.2.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/main.js"></script>
	<script src="js/wow.min.js"></script>

	<script src="http://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-3d.js"></script>
	<script type="text/javascript" src="<?= url(); ?>/js/timeline.js"></script>
	<script type="text/javascript" src="<?= url(); ?>/js/Chart.js"></script>
	<script type="text/javascript">
		var base_url = '<?= url(); ?>';
		$(document).ready(function () {
			setBaseUrl(base_url);
			getTimeline();
			getTimelineGraph();
			getTimelineGraphUserMention();
			getGraphDailyNumber();
			getGraphHashtags();
			// add a handler to the search button.
			$('#actionSearch').on('click', search);
		});
	</script>
</head>
<body>

<header class="header">
		<h1><p><span>
			<img src="img/logo.png"></span><span><font color=#ffffff>Network Institute Portal</font></span></p></h1>
</header>

<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6">
		
				<div id="tweetsGraph">
					<div>
						<div id="piechart_tweets" style="width:500px; height:350px"></div>
					</div>
					<div>
						<div id="piechart_usermention" style="width:500px; height:350px;"></div>
					</div>
					<div id="line_chart" style="width:100%; height:600px;"></div>
					<div id="hashtags_chart" style="width:100%; height:600px;"></div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
			
				<form id="searchForm" class="form-inline">
					<div class="col-md-4">
						<input type="text" class="form-control" id="keywords" placeholder="Keywords (ex. 'PhD')">
					</div>
					<div class="col-md-4">
						<input type="text" class="form-control" id="owner" placeholder="Owner (ex. 'VU')">
					</div>
					<div class="col-md-4">
						<button id="actionSearch" class="btn btn-primary btn-block"><b>Search Tweets</b></button>
					</div>
				</form>
		
				<div id="tweets" style="margin-top:70px;">

					<table class="table table-condensed">
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<footer>
	<div class="container text-center">
		<div class="row">
			<div class="col-md-12">
				<strong><br><br>
		<h3><font color=white>&copy Copyright 2016 - Social Web VU (Group 5) 
	</font>		</h3></strong>

			</div>
		</div>
	</div>
</footer>

</body>
</html>
