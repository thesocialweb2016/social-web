<?php

namespace App\Component;

use Abraham\TwitterOAuth\TwitterOAuth;
use App\Component\TweetWrapper;

class TwitterAPI {
	// user account for the twitter API.
	public $display_name = 'SocialWeb2016';
	public $consumer_key = 'tDfIcFkBJkBclKsGGyclvGeqi';
	public $consumer_secret = 'jGNUVZjlgJJ6PteehvQX4lvTjVdiorFF5za5No0c0ohEy3tNdw';
	public $access_token = '705095179838029824-4UZnDiY9G28p09fUGdTnYtdwCOg3GkF';
	public $access_token_secret = 'Ep6qE4rfRZl9GjfUysNTZF0Ox7StdYdKMvZ2udJ1H1Ruy';

	public $connection = NULL;
	public $resource_urls = [
		'home_timeline' => 'statuses/home_timeline',
	];

	// for caching purpose.
	public $tweet_cache = NULL; // the cache file.
	public $cache_time = 300; // 5 minutes cache time.

	// constructor.
	function __construct() {
		$this->connection = new TwitterOAuth(
			$this->consumer_key,
			$this->consumer_secret,
			$this->access_token,
			$this->access_token_secret
		);
		// set the default cache location.
		$this->tweet_cache = dirname(__DIR__) . '..\..\storage\app\tweet_cache.txt';
	}

	// to retrieve tweets from twitter.
	public function getTweets($_count) {
		$valid_cache = FALSE;
		// check whether the cache file exists or not.
		if (file_exists($this->tweet_cache) && filesize($this->tweet_cache)) {
			// the maximum cache time limit = the last modified time + cache time.
			$cache_limit_time = filemtime($this->tweet_cache) + $this->cache_time;

			// checking the cache time validity.
			if ($cache_limit_time >= time()) {
				$valid_cache = TRUE;
			}
		} else { // when the cache does not exist.
			// create an empty file.
			touch($this->tweet_cache);
		}

		// prepare a variable to store the tweets.
		$tweets = [];
		// when the cache is outdated, retrieve a new one from tweeter.
		if (!$valid_cache) {
			$tweets = $this->connection->get(
				$this->resource_urls['home_timeline'],
				["count" => $_count]
			);

			// convert the tweets into PHP-array instead of native object (stdClass).
			// to store it, we have to serialize it first.
			$tweets = json_decode(json_encode($tweets), TRUE);
			// store the tweets into the cache.
			file_put_contents($this->tweet_cache, serialize($tweets));
		} else { // when not outdated, then read the file.
			// read from the cache and unserialize it back.
			$tweets = unserialize(file_get_contents($this->tweet_cache));
		}

		return ($tweets);
	}

	// filter the tweets.
	public function filterTweets($_raw_tweets = [], $_filter = []) {
		// filtered tweets are equal to all the tweets, unless some filters applied to them.
		// we assume that at the beginning there is no filter is applied.
		$filtered_tweets = $_raw_tweets;

		// see if we want to filter the tweets by keywords?
		if (array_key_exists('keywords', $_filter) && !empty($_filter['keywords'])) {
			// we reset the filtered tweets to a zero-element.
			$filtered_tweets = [];

			// then for each raw tweet, we check the keywords existence insensitively.
			foreach ($_raw_tweets as $raw_tweet) {
				if (stripos($raw_tweet['text'], $_filter['keywords']) !== FALSE) {
					// each time we find a keyword existence, we put that tweet into the filtered tweets.
					$filtered_tweets[] = $raw_tweet;
				}
			}
		}

		// checking whether we are going to filter the tweets based on a screen name?
		if (array_key_exists('owner', $_filter) && !empty($_filter['owner'])) {
			// make the previous filtered tweets as the input to this process by switching their value with raw tweet.
			$_raw_tweets = ($filtered_tweets) ? $filtered_tweets : $_raw_tweets;
			$filtered_tweets = [];

			// again, for each raw tweet, we check the desired screen name.
			foreach ($_raw_tweets as $raw_tweet) {
				if (strpos($raw_tweet['user']['screen_name'], $_filter['owner']) !== FALSE) {
					$filtered_tweets[] = $raw_tweet;
				}
			}
		}

		// done, return the tweets.
		return ($filtered_tweets);
	}

	
	
	// retrieve tweets from user's home timeline.
	// 800 is the maximum number of tweets we can retrieve.
	// see https://dev.twitter.com/rest/reference/get/statuses/home_timeline
	public function getHomeTimeline($_filter = [], $_wrap = TRUE, $_count = 800) {
		// retrieve the tweets from the data source (either from twitter.com or cached file).
		$tweets = $this->getTweets($_count);

		// if the filter parameters are included, then filter the tweets.
		if ($tweets && $_filter) {
			$tweets = $this->filterTweets($tweets, $_filter);
		}

		// when wrapping is needed, then wrap them.
		if ($tweets && $_wrap) {
			$tweets = $this->wrapTweets($tweets);
		}

		return ($tweets);
	}

	// wrapping the tweets into a wrapped object.
	// see \app\component\TweetWrapper class.
	public function wrapTweets($_raw_tweets = []) {
		$wrappedTweets = [];
		if ($_raw_tweets) {
			foreach ($_raw_tweets as $tweet) {
				$wrappedTweet = TweetWrapper::extract($tweet);
				if ($wrappedTweet) {
					$wrappedTweets[] = $wrappedTweet;
				}
			}
		}

		return ($wrappedTweets);
	}

	public function getHomeTimelineGraph($_filter = [], $_wrap = FALSE, $_count = 800) {
		$timeline_list = [];
		$screen_names = [];
		$screen_name_str = [];
		$timeline_list = $this->getHomeTimeline($_filter, $_wrap, $_count);

		//convert to associative array
		$timeline_list = json_decode(json_encode($timeline_list), TRUE);

		//get screen name
		foreach($timeline_list as $timeline){
			$screen_name = $timeline['user']['screen_name'];
			if(!array_key_exists($screen_name, $screen_names)){
				$screen_names[$screen_name] = 1;
				$screen_name_str[] = $screen_name;
			} else {
				++$screen_names[$screen_name];
			}
		}

		//combine the array of user mention and the value
		$combine_name_value = array_combine($screen_name_str,$screen_names);
		//sort the array and get the top five
		arsort($combine_name_value);
		$top_five_user = array_slice($combine_name_value, 0,5);

		$topfiveuser = [];
		foreach($top_five_user as $key => $val){
			$topfiveuser[] = [
				"name"=> $key,
				"y"=> $screen_names[$key],
				"url" => "http://www.twitter.com/{$key}"
			];
		}

		$data = array('topfivetweets' => $topfiveuser );

		return($data);
	}

	public function getGraphUserMention($_filter = [], $_wrap = FALSE, $_count = 800) {
		$timeline_list = [];
		$screen_names = [];
		$screen_name_str = [];

		$timeline_list = $this->getHomeTimeline($_filter, $_wrap, $_count);
		//convert to associative array
		$timeline_list = json_decode(json_encode($timeline_list), TRUE);

		//get user mention
		foreach($timeline_list as $timeline){
			$tweet_entities = $timeline['tweet']['entities']['user_mentions'];
			foreach ($tweet_entities as $user_mention){
				$usermention = $user_mention['screen_name'];
				if(!array_key_exists($usermention, $screen_names)){
					$screen_names[$usermention] = 1;
					$screen_name_str[] = $usermention;
				}else{
					++$screen_names[$usermention];
				}
			}
		}

		//combine the array of user mention and the value
		$combine_mention_value = array_combine($screen_name_str,$screen_names);
		//sort the array and get the top five
		arsort($combine_mention_value);
		$top_five_mention = array_slice($combine_mention_value, 0,5);

		//set the data format according to format table in funtion buildGraphUserMention in timeline.js
		$topfivemention = [];
		foreach($top_five_mention as $key => $val){
			$topfivemention[] = [
				"name"=> $key,
				"y"=> $screen_names[$key],
				"url" => "http://www.twitter.com/{$key}"
			];
		}

		$data = array('topfivemention' => $topfivemention);

		return($data);
	}

	public function getGraphDailyNumber($_filter = [], $_wrap = FALSE, $_count = 800) {
		$timeline_list = [];
		$days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
		$number_tweets = [];
		date_default_timezone_set('Europe/Amsterdam');

		$day_of_week = date('w');
		for ($i = 0; $i < $day_of_week; $i++) {
			array_push($days, array_shift($days));
		}
		
		$today = new \DateTime();
		$timeline_list = $this->getHomeTimeline($_filter, $_wrap, $_count);
		//convert to associative array
		$timeline_list = json_decode(json_encode($timeline_list), TRUE);

		//get user mention
		foreach ($timeline_list as $timeline) {
			$tweet_date = $timeline['tweet']['created_at'];
			
			$date = new \DateTime($tweet_date);

			$interval = $today->diff($date);
			if ($interval->d < 6) {
				
				$split = explode(" ", $tweet_date);
				$day = $split[0];
				
				if (!array_key_exists($day, $number_tweets)) {
					$number_tweets[$day] = 1;
				} else {
					++$number_tweets[$day];
				}
				
			}
		}

		//set the data format according to format table in funtion buildGraphUserMention in timeline.js

		foreach ($days as $d) {
			$values[] = isset($number_tweets[$d]) ? $number_tweets[$d] : 0;
		}
		$data = [
			"days" => $days,
			"number_tweets" => $values,
			"day"=>$day_of_week,
			"interval"=>$interval

		];

		return ($data);

	}

	public function getGraphHashtags($_filter = [], $_wrap = FALSE, $_count = 800) {
		$timeline_list = [];
		$screen_names = [];
		$hashtags_texts = [];
		$timeline_list = $this->getHomeTimeline($_filter, $_wrap, $_count);
		$timeline_list = json_decode(json_encode($timeline_list), TRUE);
		
		foreach ($timeline_list as $tweet) {
			$screen_name = $tweet['user']['screen_name'];
			$hashtags = $tweet['tweet']['entities']['hashtags'];
			foreach ($hashtags as $hashtag) {
				$text = $hashtag['text'];
				if (!array_key_exists($screen_name, $screen_names)) {
					$screen_names[$screen_name][$text] = 1;
				} else {
					if (!array_key_exists($text, $screen_names[$screen_name])) {
						$screen_names[$screen_name][$text] = 1;
					} else {
						++$screen_names[$screen_name][$text];
					}
				}
				if (!array_key_exists($text, $hashtags_texts)) {
					$hashtags_texts[$text] = 1;
				} else {
					++$hashtags_texts[$text];
				}
			}
		}
		
		arsort($hashtags_texts);
		
		$hashtags_texts = array_slice($hashtags_texts, 0, 10, TRUE);
		$users = [];
		foreach ($screen_names as $key => $val) {
			$values = [];
			foreach ($hashtags_texts as $hashtag_text => $hashtag_count) {

				$values[] = isset($val[$hashtag_text]) ? $val[$hashtag_text] : 0;
			}
			if (array_sum($values) != 0) {
				$users[$key] = $values;
			}

		}
		
		$number_hashtags = [];
		foreach ($users as $key => $val) {
			$number_hashtags[] = [
				"name" => $key,
				"data" => $val,
			];
		}
		$data = [
			'hashtags' => array_keys($hashtags_texts),
			'number_hashtags' => $number_hashtags,

		];

		return ($data);

	}
	
public function getDayTweets($_filter = [], $_wrap = TRUE, $_count = 800) {
		// retrieve the tweets from the data source (either from twitter.com or cached file).
		$tweets = $this->getTweets($_count);

		foreach ($tweets as $tweet) {
			$today = new \DateTime();
			$tweet_date = $tweet['created_at'];
			$tweet_day = new \DateTime($tweet_date);
			$interval = $today->diff($tweet_day);
				if($interval->d<6){
				
					$split = explode(" ", $tweet_date);
					$day = $split[0];
				
			        if($day==$_filter['day'])  

					$filtered_tweets[] = $tweet;
				}
			}
	
		

		// when wrapping is needed, then wrap them.
		if ($filtered_tweets && $_wrap) {
			$filtered_tweets = $this->wrapTweets($filtered_tweets);
		}

		return ($filtered_tweets);
	}
	
}



