<?php

namespace App\Component;

class TweetWrapper {

	public static $url = 'http://www.twitter.com/';
	public static $tweet_must_have_keys = [
		'created_at',
		'id_str',
		'text',
		'geo',
		'coordinates',
	];
	public static $user_must_have_keys = [
		'id_str',
		'screen_name',
		'profile_image_url',
	];

	public $tweet = NULL;
	public $user = NULL;

	// constructor.
	function __construct($_params) {
		$this->tweet = $_params;
		$this->user = $this->tweet['user'];
		unset($this->tweet['user']);
	}

	// validate whether an input array is a valid tweet.
	public static function validate($_params, $_must_have_keys = NULL) {
		// if it is an array ...
		$is_valid = is_array($_params);
		// for iteration though the input array.
		$idx = count($_must_have_keys) - 1;

		// an input array can only be valid if it has all the 'must have keys', otherwise it is not a valid one.
		while ($is_valid && $idx >= 0) {
			$is_valid = array_key_exists($_must_have_keys[$idx--], $_params);
		}

		return ($is_valid);
	}

	// this function utilizes the validate() function specifically for tweet  validation.
	public static function validateTweet($_params) {
		return (
			self::validate($_params, self::$tweet_must_have_keys) &&
			self::validate($_params['user'], self::$user_must_have_keys)
		);
	}

	// this function utilizes the validate() function specifically for tweet's user validation.
	public static function extract($_params) {
		if (self::validateTweet($_params)) {
			return (new self($_params));
		}

		return (NULL);
	}

	// create a tweet url.
	public function getTweetUrl() {
		return ($this->getUserUrl() . '/status/' . $this->tweet['id_str']);
	}

	// create a user url.
	public function getUserUrl() {
		return (self::$url . $this->user['screen_name']);
	}
}