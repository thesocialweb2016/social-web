<?php

namespace App\Http\Controllers;

use App\Component\TwitterAPI;

class HomeController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}
	//test home controller
	public function index() {
		return view('index');
	}

	public function test() {
		$data = null;
		return view('test', ['data' => $data]);
	}
}
