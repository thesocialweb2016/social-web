var base_url = '';
function setBaseUrl(_base_url) {
    base_url = _base_url;
}

function buildRows(tweets) {
    var twitter_url = 'http://www.twitter.com/';
    // each facility is represented as a table row
    var rows = "";

    // see the data structure of parking facility in the '/data' directory.
    $.each(tweets, function () {
        var tweet = this.tweet;
        var user = this.user;

        var user_url = twitter_url + user.screen_name;
        var tweet_url = user_url + '/status/' + tweet.id_str;

        var user_cell = '<td><a href="' + user_url + '"><img src="' + user.profile_image_url + '"></a></td>';
        var tweet_cell = '<td><a href="' + tweet_url + '">' + tweet.text + '</a></td>';
        var third_cell = '<td>' + tweet.created_at + '</td>';

        rows += '<tr>' + user_cell + tweet_cell + third_cell + '</tr>';
    });

    return (rows);
};

function getTimeline() {
    // call a resource to retrieve all the available parking facilities.
    var url = base_url + '/api/timeline';
    $.getJSON(url, function (response) {
        var rows = buildRows(response);

        // insert the retrieved facilities into the table.
        $('#tweets table tbody').html(rows);
      
    });
};

function search(event){
    event.preventDefault();

    var keywords = $('#keywords').val();
    var owner = $('#owner').val();

    var params = 'keywords=' + keywords + '&' +
        'owner=' + owner;

    var url = base_url + '/api/timeline?' + params;
    $.getJSON(url, function (response) {
        var rows = buildRows(response);
        // insert the retrieved facilities into the table.
        $('#tweets table tbody').html(rows);
      
    });
}

function buildGraph(tweets) {
    $('#piechart_tweets').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'The most active users'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
                events: {
                    click: function(event, i){
                        location.href = event.point.url;
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Number of tweets',
            data: tweets.topfivetweets
        }]
    });
};

function buildGraphUserMention(tweets) {
    $('#piechart_usermention').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Top used user mentions'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
                events: {
                    click: function(event, i){
                        location.href = event.point.url;
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Number of user mention',
            data: tweets.topfivemention
        }]
    });
};

function getTimelineGraph() {
    var url = base_url + '/api/timelineGraph';

    $.getJSON(url, function (response) {
        buildGraph(response);
    });
};

function getTimelineGraphUserMention() {
    var url = base_url + '/api/getGraphUserMention';

    $.getJSON(url, function (response) {
        buildGraphUserMention(response);
    });
};


function buildGraphDailyNumber(data) {
	$('#line_chart').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Daily number of tweets'
        },
      
        xAxis: {
            categories: data.days
        },
        yAxis: {
            title: {
                text: 'Number of tweets'
            }
        },
        plotOptions: {
             series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function() {
                            getDayTweets(this.category );
                        }
                    }
                }
            }
               
        },
        series: [{
		    name:"All Users",
            data: data.number_tweets
        }]
    });
}
function getGraphDailyNumber() {
    var url = base_url + '/api/getGraphDailyNumber';

    $.getJSON(url, function (response) {
        buildGraphDailyNumber(response);
    });
};

function getGraphHashtags() {
    var url = base_url + '/api/getGraphHashtags';

    $.getJSON(url, function (response) {
       buildGraphHashtags(response);
    });
};

function buildGraphHashtags(data)
{
	
	 $('#hashtags_chart').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Top used hashtags '
        },
        xAxis: {
            categories: data.hashtags
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of hashtags'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    },
					formatter:function(){
                    if(this.y > 0)
                        return this.y;
					}
                }
            },
			  series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            getHashtagTweets(this.category);
                        }
                    }
                }
            }
        },
        series: data.number_hashtags
    });
}
function getHashtagTweets(hashtag){
	
	  var params = 'keywords=' + hashtag ;

    var url = base_url + '/api/timeline?' + params;
    $.getJSON(url, function (response) {
        var rows = buildRows(response);
        // insert the retrieved facilities into the table.
        $('#tweets table tbody').html(rows);
       
    });
	
	
}
function getDayTweets(day){
	
	
	  var params = 'day=' + day ;

    var url = base_url + '/api/getDayTweets?' + params;
    $.getJSON(url, function (response) {
        var rows = buildRows(response);
        // insert the retrieved facilities into the table.
        $('#tweets table tbody').html(rows);
      
    });
	
	
}