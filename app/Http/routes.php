<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// routing for HTML pages.
$app->get('/', 'HomeController@index');
$app->get('/test', 'HomeController@test');

// routing for API, should return value in JSON
$app->get('/api/timeline', 'ApiController@timeline');
$app->get('/api/timelineGraph', 'ApiController@getHomeTimelineGraph');
$app->get('/api/getGraphUserMention', 'ApiController@getGraphUserMention');
$app->get('/api/getGraphDailyNumber', 'ApiController@getGraphDailyNumber');
$app->get('/api/getGraphHashtags', 'ApiController@getGraphHashtags');
$app->get('/api/getDayTweets', 'ApiController@getDayTweets');