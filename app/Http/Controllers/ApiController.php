<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Component\TwitterAPI;

class ApiController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	public function timeline(Request $_request) {
		// accepting inputs from text-fields.
		$filter = $_request->all();

		// instantiate a TwitterAPI class.
		// \app\component\TwitterAPI
		$twitterAPI = new TwitterAPI();
		// retrieve the user's home timeline.
		// user credential is set statically in the TwitterAPI class.
		$tweets = $twitterAPI->getHomeTimeline($filter);

		// convert the output into JSON format.
		return response()->json($tweets);
	}

	public function getDayTweets(Request $_request){
		
		$filter = $_request->all();
		$twitterAPI = new TwitterAPI();

		$tweets = $twitterAPI->getDayTweets($filter);
		return response()->json($tweets);
	}
	public function getHomeTimelineGraph(Request $_request) {
		$filter = $_request->all();

		$twitterAPI = new TwitterAPI();
		$tweets = $twitterAPI->getHomeTimelineGraph($filter, TRUE, 100);

		//die;
		return response()->json($tweets);
	}

	public function getGraphUserMention(Request $_request) {
		$filter = $_request->all();

		$twitterAPI = new TwitterAPI();
		$tweets = $twitterAPI->getGraphUserMention($filter, TRUE, 100);

		//die;
		return response()->json($tweets);
	}

	public function getGraphDailyNumber(Request $_request) {
		$filter = $_request->all();

		$twitterAPI = new TwitterAPI();
		$tweets = $twitterAPI->getGraphDailyNumber($filter, TRUE, 100);

		//die;
		return response()->json($tweets);
	}
	
	public function getGraphHashtags(Request $_request) {
		$filter = $_request->all();

		$twitterAPI = new TwitterAPI();
		$tweets = $twitterAPI->getGraphHashtags($filter, TRUE, 100);

		//die;
		return response()->json($tweets);
	}
}
